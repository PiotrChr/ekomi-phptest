<?php

/* message.html */
class __TwigTemplate_7f7ff01af74d488389cc3b8ffd44d19a1eed726fadb757aa0034dc8714179aab extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"message\">
\t<div id=\"messageContent\">
\t\t<p>";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : null), "html", null, true);
        echo "</p>
\t</div>
\t<div id=\"messageControls\">
\t\t<a class='btn-green' href=\"./\">BACK TO LIST</a>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "message.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 3,  19 => 1,);
    }
}
