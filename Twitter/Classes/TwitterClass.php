<?php
namespace Twitter\Classes;

use Abraham\TwitterOAuth\TwitterOAuth;
use Twitter\Modules\DbModule as Db;

class TwitterClass extends BasicAbstractClass
{

	private $connection;
	private $db;

	public function __construct($db = false) {
		parent::__construct();
		$this->connect();

		$db = (true === $db) ? true : false;

		// creates new databse connection if Object constructed with 'true'
		if ($db === true) {
			$this->db = new Db(true);
		}
		
	}

	# -------
	#	(***Twitter OAuth settings - {HOME}/settings/settings.php) #
	# -------

	public function check() {
		if (empty($this->settings['twitter']['ACCESS_TOKEN_SECRET']) || empty($this->settings['twitter']['CONSUMER_KEY']) || empty($this->settings['twitter']['CONSUMER_SECRET']) || empty($this->settings['twitter']['ACCESS_TOKEN'])) {
			return 'Please provide Twitter credentials in settings file';
		} else {
			$verify = $this->verifyCredentials();
			if ($verify === true) {
				return true;
			} else {
				return $verify;
			}
		}
	}

	private function connect() 
	{	
		$this->connection = new TwitterOAuth($this->settings['twitter']['CONSUMER_KEY'], $this->settings['twitter']['CONSUMER_SECRET'], $this->settings['twitter']['ACCESS_TOKEN'], $this->settings['twitter']['ACCESS_TOKEN_SECRET']);
	}

	public function verifyCredentials() 
	{
		$verify = $this->connection->get("account/verify_credentials");
		
		if (isset($verify->id)) {
			return true;
		} else {
			return $verify->errors[0]->message;
		}
	}

	# -------
	#	Gets tweets from user timeline #
	# -------

	public function getTweets($optionsIn = array()) 
	{
		// sets default options for connection - {HOME}/settings/settings.php)
		$options = array( 
	        'twitterUser' => $this->settings['twitter']['defaults']['user'],
	        'c' => $this->settings['twitter']['defaults']['tweetCount']
	    ) ;

    	$options = array_merge($options, $optionsIn) ;

    	$params = [
    		'screen_name' => $options['twitterUser'],
    		'count' => $options['c']
    	];

    	if (!empty($options['maxId'])) $params['max_id'] = $options['maxId'];
    	if (!empty($options['sinceId'])) $params['since_id'] = $options['sinceId'];
    	
		$tweets = $this->connection->get('statuses/user_timeline', $params);
		
		return $tweets;
	}

	# -------
	#	Gets tweet when Id provided #
	# -------

	public function getTweetById($tweetId) {
		$tweet = $this->connection->get('statuses/show',array('id' => $tweetId));
		if(!isset($tweet->errors)) {
			return $tweet;
		} else {
			return false;
		}
	}

	# -------
	#	Adds tweet to database, if not allready in #
	# -------

	public function addTweet($tweet) 
	{	
		if (!($this->db instanceof Db)) {
			echo 'Please set up Database';
			return;
		}

		$entry = [
			'tweet_id' => $tweet->id,
			'tweet_text' => $this->db->escape($tweet->text),
			'created_at' => date('Y-m-d H:i:s', strtotime($tweet->created_at)),
			'user_id' => $tweet->user->id,
			'screen_name' => $tweet->user->screen_name,
			'name' => $tweet->user->name
		];

		if (isset($tweet->retweeter_status)) {
			$entry['is_rt'] = true;
		} else {
			$entry['is_rt'] = false;
		}


		$keys = '';
		$vals = '';
		$curr = 0;

		foreach ($entry as $k => $v) {
			$comma = (count($entry)-1 == $curr) ? '' : ',';
			
			$keys .= $k . $comma;
			$vals .= '\'' . $v . '\'' . $comma;
			$curr++;
		}

		$query = 'INSERT INTO tweets (' . $keys . ') VALUES (' . $vals . ')';

		if ($this->tweetInDb($entry['tweet_id']) === false ) {
			$call = $this->db->query($query);
		} 
	}

	# -------
	#	Checks is database is set #
	# -------

	private function checkDb() {
		if (!($this->db instanceof Db)) {
			echo 'Please set up Database';
			return;
		}
	}

	# -------
	#	Fetchets all Tweets from selected Twitter user (up to 3,200 tweets) #
	# -------

	public function initTweets($upDown, $optionsIn = array()) 
	{
		if (!($this->db instanceof Db)) {
			echo 'Please set up Database';
			return;
		}

		$options = array(
	        'twitterUser' => $this->settings['twitter']['defaults']['user'],
	        'c' => $this->settings['twitter']['defaults']['tweetCount']
	    );

	    if ($upDown === true) {	// count up, count down
	    	$options['sinceId'] = $this->topBottomTweet(true);
	    } else {
	    	$options['maxId'] = $this->topBottomTweet(true);
	    }

    	$options = array_merge($options, $optionsIn);
    	$tweets = $this->getTweets($options);
    	
    	foreach($tweets as $tweet) {
			$this->addTweet($tweet);
		}
		
		if (count($tweets) > 1) {	// if there's more to it, get it
			if ($upDown === true) {
				$this->initTweets(array_merge($options,Array('sinceId' => $this->topBottomTweet(true))));
			} else {
				$this->initTweets($upDown, array_merge($options,Array('maxId' => $this->topBottomTweet(false))));
			}
			
		}
	}

	# -------
	#	Checks if tweet is allready in db #
	# -------

	public function tweetInDb($tweetId) 
	{
		$result = $this->db->query('SELECT * from tweets where tweet_id=\'' . $tweetId . '\'')->getResult();
		
		if (empty($result)) {
			return false;
		} else {
			return true;
		}
	}

	# -------
	#	Gets oldest, or newest tweet from db #
	# -------

	public function topBottomTweet($topBottom) 
	{
		if (!($this->db instanceof Db)) {
			echo 'Please set up Database';
			return;
		}

		$sort = (true === $topBottom) ? 'DESC' : 'ASC';

		$query = 'SELECT tweet_id from tweets order by created_at ' . $sort . ' LIMIT 1';
		
		$result = $this->db->query($query)->getResult();

		if (empty($result)) {
			return '';
		} else {
			return $result[0]['tweet_id'];
		}		
	}


	# -------
	#	Uncomment to use DB twitter fetcher and paginator #
	# -------
	
	/*
	public function getTweetsFromDb($pageSize,$page) {

		return $this->checkDb(); // checks if databse is set
		
		$query = 'SELECT * from tweets LIMIT ' . $pageSize . ' OFFSET ' . $page*$pageSize;
		$tweets = $this->db->query('SELECT * from tweets ')->getResult();

		return $tweets;
	}

	public function getPages($pageSize) {
		
		return $this->checkDb(); // checks if databse is set

		$query = 'SELECT * from tweets';
		$tweets = $this->db->query('SELECT * from tweets ')->getResult();

		return ceil(count($tweets) / $pageSize);		
	}
	*/

	# -------
	#	Retweet method #
	# -------

	public function retweet($tweetId) {
		$retweet = $this->connection->post('statuses/retweet/'.$tweetId);
		if(!isset($retweet->errors)) {
			return true;
		} else {
			return false;
		}
	}

	# -------
	#	Reply method #
	# -------

	public function replyTweet($replyText, $tweetId) 
	{
		$reply = $this->connection->post('statuses/update', array('status' => $replyText,'in_reply_to_status_id' => $tweetId));

		if(!isset($reply->errors)) {
			return true;
		} else {
			return false;
		}
	}

	# -------
	#	Favorite method #
	# -------

	public function favoriteTweet($tweetId) 
	{
		$favorite = $this->connection->post('favorites/create',array('id' => $tweetId));
		if(!isset($favorite->errors)) {
			return true;
		} else {
			return false;
		}
	}
}