<?php
namespace Twitter\Classes;

abstract class BasicAbstractClass
{
	public $settings;

	public function __construct() {
		global $settings;
		$this->settings = $settings;
	}
}