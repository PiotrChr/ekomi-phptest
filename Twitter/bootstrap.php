<?php
namespace Twitter;

require_once __DIR__ . '\Common\AutoLoader.php';
require __DIR__ . '\vendor\autoload.php';

$autoloader = new \Twitter\Common\AutoLoader(__NAMESPACE__, dirname(__DIR__));
$autoloader->register();