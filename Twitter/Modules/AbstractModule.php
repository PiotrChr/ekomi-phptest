<?php
namespace Twitter\Modules;

abstract class AbstractModule 
{
	public $settings;

	public function __construct() {
		global $settings;
		$this->settings = $settings;
	}
}