<?php
namespace Twitter\Modules;
	
class DbModule extends AbstractModule 
{
	
	protected $dbPassword;
	protected $dbUser;
	protected $dbHost;
	public $result;
	protected $mysqli;
	
	public function __construct($defaults) 
	{
		parent::__construct();
		if ($defaults === true) {
			$this->setDefaults();
		}
		
		$this->mysqli = new \mysqli($this->dbHost, $this->dbUser, $this->dbPassword, $this->dbName);
			if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}
			return $this;
	}
		
	# -------
	#	(***MySQL settings - {HOME}/settings/settings.php) #
	# -------

	public function setDefaults() 
	{
		$this->dbPassword = $this->settings['mysql']['mysql_password'];
		$this->dbName = $this->settings['mysql']['mysql_database'];
		$this->dbUser = $this->settings['mysql']['mysql_user'];
		$this->dbHost = $this->settings['mysql']['mysql_host'];
	}
		
	public function query($query) 
	{
		$this->result = $this->mysqli->query($query);
		return $this;
	}

	public function escape($string) 
	{
		return $this->mysqli->real_escape_string($string);
	}
		
	public function affectedRows() 
	{
		return $this->mysqli->affected_rows;
	}
		
	public function getResult() 
	{
		//$this->mysqli->close();
		$result = [];
		if ($this->result) {
			while ($row = $this->result->fetch_assoc()) {
				$result[] = $row;
			}
		}
		return $result;
	}
}