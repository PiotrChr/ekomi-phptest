<?php
namespace Twitter\Common;

class AutoLoader
{
    protected $namespace = '';

    protected $path = '';

    # -------
    #   Build instance of autoloader #
    # -------
    public function __construct($namespace, $path)
    {
        $this->namespace = ltrim($namespace, '\\');
        $this->path      = rtrim($path, '/\\') . DIRECTORY_SEPARATOR;
    }

    # -------
    #   Try to load class #
    # -------
    public function load($class)
    {
        $class = ltrim($class, '\\');

        if (strpos($class, $this->namespace) === 0) {
            $nsparts   = explode('\\', $class);
            $class     = array_pop($nsparts);
            $nsparts[] = '';
            $path      = $this->path . implode(DIRECTORY_SEPARATOR, $nsparts);
            $path     .= str_replace('_', DIRECTORY_SEPARATOR, $class) . '.php';

            if (file_exists($path)) {
                require $path;
                return true;
            }
        }
        return false;
    }

    # -------
    #   Register autoloader to php #
    # -------
    public function register()
    {
        return spl_autoload_register(array($this, 'load'));
    }

    # -------
    #   And unregister #
    # -------
    public function unregister()
    {
        return spl_autoload_unregister(array($this, 'load'));
    }
}