<?php

namespace Twitter\Views;

abstract class AbstractView
{
	protected $template_dir = 'templates/';
	protected $vars = array();
}