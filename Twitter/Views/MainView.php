<?php

namespace Twitter\Views;

class MainView extends AbstractView
{
	public $twig;

    public function __construct($template_dir = null) 
    {
        # -------
        #   Template paths provided by AbstractView #
        # -------

        if ($template_dir !== null) {
            // Check here whether this directory really exists
            $this->template_dir = $template_dir;
        }

        # -------
        #   Load Twig templating engine #
        # -------

        $twigLoader = new \Twig_Loader_Filesystem($this->template_dir);
        $this->twig = new \Twig_Environment($twigLoader, array(
		    'cache' => 'tmp/cache',
		));
    }

    # -------
    #   Show custom message #
    # -------

    public function message($message) 
    {
        return $this->twig->render('message.html', array('message' => $message));
    }

    # -------
    #   Show Tweets #
    # -------

    public function renderTweets() 
    {
    	return $this->twig->render('tweet.html', array('tweets' => $this->vars['tweets']));
    }

    # -------
    #   Show Reply Form #
    # -------

    public function renderReplyForm() {
        return $this->twig->render('reply.html', array('tweet' => $this->vars['tweet']));
    }

    # -------
    #   Show Retweet Confirmation Box #
    # -------

    public function renderRetweetConfirmation() {
        return $this->twig->render('confirm.html', array(
                'tweet' => $this->vars['tweet'],
                'message' => 'Do you really wan\'t to retweet this tweet?',
                'action' => 'retweet'
            ));
    }

    # -------
    #   Show Favorite Confirmation Box #
    # -------

    public function renderFavoriteConfirmation() {
        return $this->twig->render('confirm.html', array(
                'tweet' => $this->vars['tweet'],
                'message' => 'Do you really wan\'t to favorite this tweet?',
                'action' => 'favorite'
            ));
    }

    public function __set($name, $value) 
    {
        $this->vars[$name] = $value;
    }

    public function __get($name) 
    {
        return $this->vars[$name];
    }
}