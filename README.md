This is a PHP Test for eKomi company.


It's quasi MVC and Controller part play here index.php file.


I've used Twig templater which is caching content in order to speed up responses. When updating template files excessively please remember to clean up cache dir (tmp/cache).



You can find database dump, and schema in the repository as well.

Schema -> mysql_database_schema.sql

Dump -> dump.sql




**To get it running please fill settings.php with your credentials and database configuration**

-> settings/settings.php


```
#!php

<?php
	$settings = [
		'paths' => [
		],
		'twitter' => [
			'CONSUMER_KEY' => '',
			'CONSUMER_SECRET' => '',
			'ACCESS_TOKEN' => '',
			'ACCESS_TOKEN_SECRET' => '',
			'defaults'=> [
				'tweetCount'=> 200,
				'user' => 'ekomi'
			]
		],
		'mysql' => [
			'mysql_password' => '',
			'mysql_database' => '',
			'mysql_user' => '',
			'mysql_host' => ''
		]
	];
```

**I strongly recommend keeping tweetCount at 200, as Twitter has it's requests limit for every 15 min and lower tweetCount means more iterations while fetching.**

For quick preview purposes I attach few images I made running on localhost with my own configuration.


Main Screen with CSS dropdown to choose number of tweets fetched from server. 
![mainscreen.jpg](https://bitbucket.org/repo/oKeedo/images/2135955069-mainscreen.jpg)

Reply screen
![replyscreen.jpg](https://bitbucket.org/repo/oKeedo/images/4098405437-replyscreen.jpg)

Simple confirmation modal
![favoritetweet.jpg](https://bitbucket.org/repo/oKeedo/images/3836810065-favoritetweet.jpg)

![confirmation.jpg](https://bitbucket.org/repo/oKeedo/images/985830698-confirmation.jpg)

If there will be some errors, modal will appear as well. 
![tweetnoexists.jpg](https://bitbucket.org/repo/oKeedo/images/3021240726-tweetnoexists.jpg)

![ratelimit.jpg](https://bitbucket.org/repo/oKeedo/images/3193189443-ratelimit.jpg)