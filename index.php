<?php

include 'settings/settings.php';
require_once __DIR__ . '\Twitter\bootstrap.php';

use \Twitter\Classes\TwitterClass as Twitter;
use \Twitter\Views\MainView as Main;

$_GET = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);

if (isset($_GET['count'])) {
	$count = $_GET['count'];
} else {
	$count = 10;
}

$twitter = new Twitter(false); // fire with 'true' parameter to add support for database
$view = new Main();

#---------
# Fire to fill empty database | Note: Twitter Api support up to 3,200 last results #
#--------

//$options = ['twitterUser'=>'eKomi','c' => 200];
//$tweets = $twitter->initTweets(true, $options); 


# -------
#   Content controller (fires when credentials for Twitter are in settings.php file) #
# -------

function showItAll() 
{
	global $twitter, $count, $view;

	if (isset($_GET['action']) && isset($_GET['id'])) {

		$action = $_GET['action'];
		$id = $_GET['id'];

		if (!isset($_GET['confirm'])) {

			$tweet = $twitter->getTweetById($id);

			if ($tweet === false) {

				echo $view->message('Tweet does not exist');

			} else {

				$view->tweet = $tweet;

				# -------
				#   Render reply form, favorite confirmation or retweet confirmation according to action #
				# -------

				switch ($action) {
					case 'reply':
						echo $view->renderReplyForm();
						break;
					case 'favorite':
						echo $view->renderFavoriteConfirmation();
						break;
					case 'retweet':
						echo $view->renderRetweetConfirmation();
						break;
				}
			}	
		} else if (isset($_GET['confirm'])) {

			switch ($action) {
				case 'reply':

					# -------
					#   If replied, show message, or throw error #
					# -------
								
					$text = $_GET['text'];
					if ($twitter->replyTweet($text,$id) === true) {
						echo $view->message('Reply sent. Thanks!');
					} else {
						echo $view->message('Error occured, try another time maybe');
					}
					break;
				case 'favorite':

					# -------
					#   If favorited, show message, or throw error #
					# -------

					if ($twitter->favoriteTweet($id) === true) {
						echo $view->message('Tweet added to favorites. Thanks!');
					} else {
						echo $view->message('Error occured, try another time maybe');
					}
					break;
				case 'retweet':

					# -------
					#   If retweeted, show message, or throw error #
					# -------

					if ($twitter->retweet($id) === true) {
						echo $view->message('Tweet retweeted. Thanks!');
					} else {
						echo $view->message('Error occured, try another time maybe');
					}
						break;
			}
		}
	} else {

		# -------
		#   If no actions, show regular tweet list #
		# -------

		$view->tweets = $twitter->getTweets(['c' => $count]);
		echo $view->renderTweets();
	}
}

?>

<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>eKomi PHP Test</title>
        <meta name="description" content="eKomi PHP Test">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/main.css">
    </head>
	<body>
		<div id="topLogo">
			<img src="https://pbs.twimg.com/profile_banners/19068239/1374067796/1500x500">
		</div>

		<div id="bodyWrapper">
			<div id="bodyContainer">
				<?php
					$check = $twitter->check();
					if ($check === true) {
						showItAll();
					} else {
						echo $view->message($check);
					}
				?>
			</div>
		</div>
	</body>
</html>
