<?php
	$settings = [
		'paths' => [
		],
		'twitter' => [
			'CONSUMER_KEY' => '',
			'CONSUMER_SECRET' => '',
			'ACCESS_TOKEN' => '',
			'ACCESS_TOKEN_SECRET' => '',
			'defaults'=> [
				'tweetCount'=> 200,
				'user' => 'eKomi'
			]
		],
		'mysql' => [
			'mysql_password' => '',
			'mysql_database' => '',
			'mysql_user' => '',
			'mysql_host' => ''
		]
	];